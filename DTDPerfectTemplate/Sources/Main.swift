//
//  main.swift
//  PerfectTemplate
//
//  Created by Kyle Jessup on 2015-11-05.
//	Copyright (C) 2015 PerfectlySoft, Inc.
//
//===----------------------------------------------------------------------===//
//
// This source file is part of the Perfect.org open source project
//
// Copyright (c) 2015 - 2016 PerfectlySoft Inc. and the Perfect project authors
// Licensed under Apache License v2.0
//
// See http://perfect.org/licensing.html for license information
//
//===----------------------------------------------------------------------===//
//
import PerfectLib
import PerfectHTTP
import PerfectHTTPServer

let jsonConfig: String = "{" +
    "\"timeForRequest\": 120," +
    "\"countForRequest\": 20," +
    "\"eventParamsCount\": 10," +
    "\"sessionTimeout\": 10," +
    "\"serverTime\": 12346517324," +
    "\"exclude\": { " +
        "\"events\": {" +
            "\"pl\": [" +
                "\"key1\",\"key2\"" +
            "]," +
            "\"ce\": [" +
                "\"name1\",\"name2\"" +
            "]," +
            "\"tr\": [" +
                "1,2,3" +
            "]," +
            "\"ip\": [" +
                "\"inAppId1\",\"inAppId2\"" +
            "]," +
            "\"sc\": [" +
                "\"vk\", \"tw\"" +
            "]," +
            "\"sp\": []\"" +
        "}," +
        "\"version\": 1," +
        "\"result\": 0" +
    "}," +
    "\"userCounting\": true" +
"}";

// An example request handler.
// This 'handler' function can be referenced directly in the configuration below.
func handler(data: [String:Any]) throws -> RequestHandler {
    return {
        request, response in
        // Respond with a simple message.
        response.setHeader(.contentType, value: "text/html")
        response.appendBody(string: "<html><title>Hello, world!</title><body>Hello, world!</body></html>")
        // Ensure that response.completed() is called when your processing is done.
        response.completed()
    }
}

func configHandler(data: [String:Any]) throws -> RequestHandler {
    return {
        request, response in
        // Respond with a simple message.
        response.setHeader(.contentType, value: "application/json")
        response.appendBody(string: jsonConfig)
        // Ensure that response.completed() is called when your processing is done.
        response.completed()
    }
}

// Configuration data for an example server.
// This example configuration shows how to launch a server
// using a configuration dictionary.

let confData = [
    "servers": [
        // Configuration data for one server which:
        //	* Serves the hello world message at <host>:<port>/
        //	* Serves static files out of the "./webroot"
        //		directory (which must be located in the current working directory).
        //	* Performs content compression on outgoing data when appropriate.
        [
            "name":"localhost",
            "port":8181,
            "routes":[
                ["method":"get", "uri":"/v2/analytics/config/", "handler":configHandler],
                ["method":"get", "uri":"/", "handler":handler],
                ["method":"get", "uri":"/**", "handler":PerfectHTTPServer.HTTPHandler.staticFiles,
                 "documentRoot":"./webroot",
                 "allowResponseFilters":true]
            ],
            "filters":[
                [
                    "type":"response",
                    "priority":"high",
                    "name":PerfectHTTPServer.HTTPFilter.contentCompression,
                    ]
            ]
        ]
    ]
]

do {
    // Launch the servers based on the configuration data.
    try HTTPServer.launch(configurationData: confData)
} catch {
    fatalError("\(error)") // fatal error launching one of the servers
}
